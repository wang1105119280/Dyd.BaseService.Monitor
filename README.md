##分支说明##

该分支是基于BSF的基础上开发的分支。<br/>
修改内容<br/>
1）sdk以插件的形式扩展自BSF。<br/>
2）项目命名空间从Dyd.Base.Monitor修改为Monitor<br/>
3) 打包安装包，可以直接被第三方安装使用。<br/>
4）若使用旧版本XXF.dll，请则使用master分支<br/>

直接使用**“监控平台安装包”**下的安装包直接安装试用。<br/>

##.net 监控平台
用于集群的性能监控,应用耗时监控管理，统一日志管理等多维度的性能监控分析。

##集群的性能监控:（需要服务器部署监控节点）
 1. 目前仅支持windows服务器监控,
 2. 支持windows自带的性能分析器所有配置项监控，及预警的定制。
 3. 支持自定义的插件扩展服务器采集dll进行各类性能采集及预警的定制。
 4. 支持性能采集快照。
 5. 支持cpu,内存，磁盘读写，网络上传下载，iis请求这些基础的服务器性能图展示。

##应用耗时监控管理:（需要应用在api和sql层嵌入sdk进行拦截）
  1. 支持sql的耗时（均值，最大值，最小值，查询次数）性能分析及性能图展示。
  2. 支持api接口的耗时（均值，最大值，最小值，查询次数）性能分析及性能图展示。
  3. 支持api与api内部sql的对照参照分析。
  4. 问题sql：未进行参数化sql的分析列表。

##统一日志管理：（需要应用集成sdk）
  1. 整个平台的错误日志集中管理及预警邮件发送。
  2. 整个平台的普通日志集中管理。

##监控平台任务:(需要将"任务"挂载在"任务调度平台"开源项目)
  1. 统计SQLHASH和URL的对照
  2. 统计服务器监控信息
  3. 自动创表
  4. 统计SQLHASH对照和SQL执行次数
  5. SQL性能监控统计
  6. API性能监控统计
  7. 耗时性能预警任务
  8. 错误频率预警任务
  9. 错误邮件发送任务
  10.集群性能预警任务

##备注：
每个公司都有特定的平台统一监控的需求及改进方案，所以开源的监控平台只是一种参考，并不适用于不同行业不同的项目。
希望此开源项目能带来更多思路,成为同类优秀项目的起点。另外项目文档及安装资料目前未整理全面，后续完善。

未来构想：
1. 底层采用nosql等其他存储进行大容量数据采集及分析。

-- 车江毅 2015-07-23 

开源相关群: .net 开源基础服务 851340557
(大家都有本职工作，也许不能及时响应和跟踪解决问题，请谅解。)

##.net 开源第三方开发学习路线 ##

- 路线1:下载开源源码->学习开源项目->成功部署项目（根据开源文档或者QQ群项目管理员协助）->成为QQ群相关项目管理员->了解并解决日常开源项目问题->总结并整理开源项目文档并分享给大家或推广->成为git项目的开发者和参与者
- 路线2:下载开源源码->学习开源项目->成功部署项目（根据开源文档或者QQ群项目管理员协助）->在实际使用中发现bug并提交bug给项目相关管理员
- 路线3:下载开源源码->学习开源项目->成功部署项目（根据开源文档或者QQ群项目管理员协助）->自行建立开源项目分支->提交分支新功能给项目官方开发人员->官方开发人员根据项目情况合并新功能并发布新版本

## 关于.net 开源生态圈的构想 ##
<b>.net 生态闭环</b>：官方开源项目->第三方参与学习->第三方改进并提交新功能或bug->官方合并新功能或bug->官方发布新版本<br/>
<b>为什么开源?</b> .net 开源生态本身弱,而强大是你与我不断学习，点滴分享,相互协助，共同营造良好的.net生态环境。<br/>
<b>开源理念:</b> 开源是一种态度，分享是一种精神，学习仍需坚持，进步仍需努力，.net生态圈因你我更加美好。<br/>

作者：车江毅

<p>部分截图：</p>

<p><img src="http://static.oschina.net/uploads/space/2015/0925/093103_bcgb_2379842.png" /></p>

<p><img src="http://static.oschina.net/uploads/space/2015/0925/093103_VpEi_2379842.png" /></p>

<p><img src="http://static.oschina.net/uploads/space/2015/0925/093104_EbIv_2379842.png" /></p>

<p><img src="http://static.oschina.net/uploads/space/2015/0925/093104_Tcjj_2379842.png" /></p>

<p><img src="http://static.oschina.net/uploads/space/2015/0925/093104_ytKa_2379842.png" /></p>

<p><img src="http://static.oschina.net/uploads/space/2015/0925/093104_MP8Z_2379842.png" /></p>

<p><img src="http://static.oschina.net/uploads/space/2015/0925/093104_7c7e_2379842.png" /></p>

<p><img src="http://static.oschina.net/uploads/space/2015/0925/093105_JH8f_2379842.png" /></p>

<p><img src="http://static.oschina.net/uploads/space/2015/0925/093105_TZpC_2379842.png" /></p>

<p><img src="http://static.oschina.net/uploads/space/2015/0925/093105_jQpt_2379842.png" /></p>

<p><img src="http://static.oschina.net/uploads/space/2015/0925/093105_bG2G_2379842.png" /></p>

<p><img src="http://static.oschina.net/uploads/space/2015/0925/093105_hPuo_2379842.png" /></p>

<p>&nbsp;</p>