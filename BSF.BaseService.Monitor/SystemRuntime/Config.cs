﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSF.BaseService.Monitor.Dal;
using BSF.Config;
using BSF.Db;


namespace BSF.BaseService.Monitor.SystemRuntime
{
    public class Config
    {
        static Config()
        {
            try
            {
                SqlHelper.ExcuteSql(BSFConfig.MonitorPlatformConnectionString, (c) => {
                    tb_database_config_dal configdal = new tb_database_config_dal();
                    UnityLogConnectString = DbShardingHelper.GetDataBase(configdal.GetModelList(c), DataBaseType.UnityLog);
                });
            }
            catch 
            {
                
            }
        }
        public static string UnityLogConnectString = "";

        public static int TimeWatchLogBatchCommitCount { get { return Convert.ToInt32(ConfigHelper.Get("TimeWatchLogBatchCommitCount", "10000")); } }
    }
}
